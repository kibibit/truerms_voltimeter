/*
*   voltimetro TrueRMS con arduino 
*   y visualizacion en Display 16x2
*/

#include <LiquidCrystal.h>

const int rs = 7, en = 6, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

uint16_t lecturas[32];
int lectura,i,j,rms;
unsigned long tiempo,suma;
float voltaje;

void setup(){
  Serial.begin(57600);
  lcd.begin(16, 2);
  lcd.print("Voltimetro RMS");
  delay(2000);
  tiempo=micros();
  i=0;
  j=0;
}
void loop(){
  //se realiza una medicion cada 250uS (4000Hz) 
  //se elimina el offset de la medida
  //se eleva al cuadrado y se suman los valores
  if(micros()-tiempo>250){
    tiempo=micros();
    lectura=analogRead(A0)-511;
    suma+=pow(lectura,2);
    i++;
  }
  //se saca la raiz cuadrada para obtener la RMS
  if(i==128){
    lecturas[j]=sqrt(suma/128);
    i=0;
    suma=0;
    j++;
  }
  //se promedian 32 valores RMS para mayor estabilidad en la medida
  if(j==32){
    for(j=0;j<32;j++){
      suma+=lecturas[j];
    }
    rms=suma/32;
    //Serial.println(rms);
    //se muestra el voltaje RMS en el display
    lcd.clear();
    lcd.setCursor(0, 1);
    lcd.print(rms);
    suma=0;
    j=0;
  }
}